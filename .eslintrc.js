module.exports = {
	parser: "@typescript-eslint/parser", // Specifies the ESLint parser

	ignorePatterns: ["src/app/public/**/*.js", "src/@types/**/*", "webpack.config.js"],

	settings: {
		react: {
			version: "detect",
		}
	},

	extends: [
		"plugin:@typescript-eslint/recommended"
	],

	rules: {
		"@typescript-eslint/no-non-null-assertion": "off",
		"@typescript-eslint/no-var-requires": "off",
	},
};
