import { readFileSync, existsSync } from "fs";
import { writeFile } from "fs/promises";


export const domainName = "rubenwardy.com";
export const ignoredDomains = [
	"weblate.rubenwardy.com",
];
const saveFilePath = process.env.SAVE_FILE_PATH ?? "refback.json";
export const logPath = process.env.NGINX_LOG_PATH ?? "/var/log/nginx/access.log";

export const ignoredDomainsEnd = [
	".rubenwardy.com",
	".minetest.net",
	".google.com",
	".duckduckgo.com",
	".bing.com",
	".brave.com",
	".startpage.com",
	".ya.ru",
	".baidu.com",
	".metager.org",
	".ecosia.org",
	".angolia.com",
	".ya.ru",
	".lycos.com",
	".qwant.com",
	".searchmysite.net",
	".you.com",
	".t.co",
	".updowntoday.com",
];

export const ignoredDomainsAny = [
	".google.",
	".yandex.",
];


export interface PersistedData {
	[domain: string]: {
		rawReferrers: {
			[url: string]: {
				[from: string]: true,
			},
		}
	},
}


export function load(): PersistedData {
	if (!existsSync(saveFilePath)) {
		return {};
	}

	const data = readFileSync(saveFilePath, { encoding: "utf-8" });
	return JSON.parse(data);
}


export async function save(persistedData: PersistedData) {
	await writeFile(saveFilePath, JSON.stringify(persistedData) + "\n",
			{ encoding: "utf-8" });
}
