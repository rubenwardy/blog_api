const { Tail } = require("tail");
import { sendToDiscord } from "../discord";
import { domainName, ignoredDomains, ignoredDomainsAny, ignoredDomainsEnd, load, logPath, save } from ".";


const persistedData = load();

const r = /^.+ ([^ ]*) [^ ]* \[[^\]]*\] "GET ([^ ]+).*" ([0-9]+) [0-9]+ "([^"]+)" "([^"]+)" [0-9\.]+$/g;
async function handleLogLine(data: string): Promise<void> {
	const parts: string[] = data.matchAll(r)?.next()?.value;
	if (!parts || parts.length < 6) {
		return;
	}

	const domain = parts[1];
	let targetURL = parts[2];
	const status = parts[3];
	const referrer = parts[4];
	const userAgent = parts[5];
	if (!domain.includes(domainName) ||
			status != "200" ||
			referrer == "-" ||
			targetURL.includes("/static/") ||
			targetURL == "/favicon.ico" ||
			targetURL.startsWith("/feed") ||
			targetURL.endsWith(".png") ||
			targetURL.endsWith(".js") ||
			targetURL.endsWith(".css")) {
		return;
	}

	// Check for ignored domains
	if (ignoredDomains.some(x => domain.includes(x))) {
		return;
	}

	// Remove all query args, to get rid of tracking rubbish
	{
		const parsed = new URL(`https://${domain}${targetURL}`);
		targetURL = parsed.pathname;
	}

	// Check referrer
	try {
		const referrerURL = new URL(referrer);
		let referrerDomain = referrerURL.hostname;
		if (referrerDomain[0] != ".") {
			referrerDomain = "." + referrerDomain;
		}

		if (ignoredDomainsEnd.some(x => referrerDomain.endsWith(x)) ||
				ignoredDomainsAny.some(x => referrerDomain.includes(x))) {
			return;
		}
	} catch (e: unknown) {
		// ignore
	}

	const domainData = persistedData[domain] ?? { rawReferrers: {} };
	persistedData[domain] = domainData;

	// Store
	const targetData = domainData.rawReferrers[targetURL] ?? {};
	domainData.rawReferrers[targetURL] = targetData;
	if (targetData[referrer]) {
		return;
	}
	targetData[referrer] = true;

	await save(persistedData);
	await sendToDiscord(`refback: ${targetURL} from ${referrer}, ${userAgent} agent`);
}


const tail = new Tail(logPath, { fromBeginning: true });

tail.on("line", function(data: string) {
	handleLogLine(data).catch(console.error);
});

tail.on("error", function(error: unknown) {
	console.log('ERROR: ', error);
});
