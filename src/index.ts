import express, { Request, Response } from "express";
import { sendToDiscord } from "./discord";

const app = express();

app.use(express.json());
app.use(express.urlencoded());

const port = process.env.PORT ?? 5000;
const VERSION = require("../package.json").version;


app.get("/", (req: Request, res: Response) => {
	res.send({ version: VERSION });
});


interface Comment {
	name: string;
	message: string;
	url: string;
	username?: string;
	redirect_to?: string;
	email: string;
}


interface Webmention {
	deleted?: boolean;
	post?: {
		"wm-property": "in-reply-to" | "like-of" | "repost-of" |
			"bookmark-of" | "mention-of" | "rsvp";
	}
	target: string;
	source: string;
}


app.post("/comment/", async (req: Request, res: Response) => {
	const payload: Comment = req.body;
	const name = payload.name || "Anonymous";
	const message = payload.message;
	if (!message) {
		res.status(400).send({ error: "Missing message"});
		return;
	}

	const url = payload.url;
	if (!url) {
		res.status(400).send({ error: "Missing url"});
		return;
	}

	const honeypot = payload.username;
	if (honeypot !== undefined && honeypot != "") {
		console.log("Rejected spam comment", new Date().toISOString());
		console.log(JSON.stringify(payload));

		res.status(500).send({ error: "Unknown server error" });
		return;
	}

	const redirectTo: (string | undefined) = payload.redirect_to;
	if (redirectTo && !isRedirectSafe(redirectTo)) {
		res.status(400).send({ error: "Attempt to redirect to invalid URL" });
		return;
	}

	await sendToDiscord(`
		**Comment on <${url}>**
		${name} ${payload.email ?? ""}

		${message}
	`);

	if (redirectTo) {
		res.redirect(redirectTo);
	} else {
		res.status(201).send({ success: true });
	}
});


app.post("/like/", async (req: Request, res: Response) => {
	const url = req.body.url;
	if (!url) {
		res.status(400).send({ error: "Missing url"});
		return;
	}

	const redirectTo: (string | undefined) = req.body.redirect_to;
	if (redirectTo && !isRedirectSafe(redirectTo)) {
		res.status(400).send({ error: "Attempt to redirect to invalid URL" });
		return;
	}

	if (redirectTo) {
		res.redirect(redirectTo);
	} else {
		res.status(201).send({ success: true });
	}
});


app.post("/subscribe/", async (req: Request, res: Response) => {
	const email = req.body.email;
	if (!email) {
		res.status(400).send({ error: "Missing email"});
		return;
	}

	const url = req.body.url;
	if (!url) {
		res.status(400).send({ error: "Missing url"});
		return;
	}

	const honeypot = req.body.username;
	if (honeypot !== undefined && honeypot != "") {
		console.log("Rejected spam subscription");
		res.status(500).send({ error: "Unknown server error" });
		return;
	}

	const redirectTo: (string | undefined) = req.body.redirect_to;
	if (redirectTo && !isRedirectSafe(redirectTo)) {
		res.status(400).send({ error: "Attempt to redirect to invalid URL" });
		return;
	}

	await sendToDiscord(`
		**Subscription on <${url}>**
		${email}
	`);

	if (redirectTo) {
		res.redirect(redirectTo);
	} else {
		res.status(201).send({ success: true });
	}
});


app.post("/webhook/", async (req: Request, res: Response) => {
	const payload: Webmention = req.body;

	if (payload.deleted) {
		res.status(202).send({ ok: true });
		return;
	}

	if (payload.post && !payload.source) {
		console.error("No source in payload", payload);
		return;
	}

	if (payload.post && payload.source && !payload.source.startsWith("https://brid.gy/")) {
		const type = payload.post["wm-property"];
		await sendToDiscord(`
			**${type}: <${payload.target}>**

			${payload.source}
		`);
	}

	res.status(201).send({ success: true });
});


app.listen(port, () => {
	console.log(`[server]: Server is running at http://localhost:${port}`);
});


function isRedirectSafe(redirectTo: string) {
	return redirectTo.startsWith("https://") &&
		new URL(redirectTo).host.endsWith("rubenwardy.com");
}
