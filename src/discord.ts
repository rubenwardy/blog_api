import { Request as FetchRequest } from "node-fetch";
import { fetchCatch } from "./fetchCatch";

const DISCORD_WEBHOOK = process.env.DISCORD_WEBHOOK!;

export async function sendToDiscord(content: string) {
	if (DISCORD_WEBHOOK == undefined) {
		console.log("Discord webhook disabled");
		console.log(content);
		return;
	}

	// Only allow at most two new lines in a row, ignoring whitespace
	content = content.replace(/\n\s*\n/g, "\n\n");

	await fetchCatch(new FetchRequest(DISCORD_WEBHOOK, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify({
			content: content.replace(/\t/g, "").substring(0, 2000),
		}),
	}));
}
