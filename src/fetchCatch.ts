import fetch, { RequestInfo, RequestInit, Request as FetchRequest, Response as FetchResponse } from "node-fetch";
import UserError from "./UserError";

export async function fetchCatch(url: RequestInfo, init?: RequestInit): Promise<FetchResponse> {
	try {
		init = init ?? {};
		if (!init.timeout) {
			init.timeout = 20000;
		}
		return await fetch(url, init);
	} catch (e) {
		let host = "?";
		if (typeof (url) == "string") {
			host = new URL(url).host;
		} else if (url instanceof FetchRequest) {
			host = new URL(url.url).host;
		}

		throw new UserError(`Error whilst connecting to ${host}`);
	}
}
